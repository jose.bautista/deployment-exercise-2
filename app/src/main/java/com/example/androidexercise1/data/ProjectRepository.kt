package com.example.androidexercise1.data

import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ProjectRepository(private val projectsDao: ProjectsDAO) {

    val getProjects: LiveData<List<Projects>> = projectsDao.getProjects()

    fun addProject(project: Projects) {
        projectsDao.addProject(project)
    }

    fun updateProject(project: Projects){
        projectsDao.updateProject(project)

    }

    fun deleteProject(project: Projects){
        projectsDao.deleteProject(project)
    }

    fun deleteAllProjects(){
        projectsDao.deleteAllProjects()
    }


}