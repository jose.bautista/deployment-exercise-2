package com.example.androidexercise1.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [Projects::class], version = 1, exportSchema = false)
abstract class ProjectsDatabase: RoomDatabase() {

    abstract fun projectsDao() : ProjectsDAO

    companion object{
        @Volatile
        private var INSTANCE : ProjectsDatabase? = null

        fun getDatabase(context: Context): ProjectsDatabase{
            val tempInstance = INSTANCE
            if(tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext, ProjectsDatabase::class.java,
                    "project_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }

}