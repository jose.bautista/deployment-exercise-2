package com.example.androidexercise1

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavArgs
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.androidexercise1.data.ProjectViewModel
import com.example.androidexercise1.data.Projects
import kotlinx.android.synthetic.main.card.view.*
import kotlinx.android.synthetic.main.fragment_2.view.*
import kotlinx.android.synthetic.main.fragment_add.*
import kotlinx.android.synthetic.main.fragment_add.view.*


class AddFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_add, container, false)

        view.button.setOnClickListener{
            insertDatatoDatabase()
        }
        return view
    }

    private fun insertDatatoDatabase() {
        val title = addTitle.text.toString()
        val description = addDesciption.text.toString()
        val long_desc = addLongDesciption.text.toString()

        if(inputCheck(title, description, long_desc)){
            val project = Projects(0,title, description, long_desc)
            viewModel.addProject(project)
            Toast.makeText(requireContext(), "Successfully added!", Toast.LENGTH_LONG).show()
            findNavController().navigate(AddFragmentDirections.actionAddFragmentToFragment1())
        }
        Toast.makeText(requireContext(), "Please Fill", Toast.LENGTH_LONG).show()
    }

    private fun inputCheck(title: String, description: String, long_description: String): Boolean {
        return !(TextUtils.isEmpty(title)&&TextUtils.isEmpty(description)&&TextUtils.isEmpty(long_description))
    }

    private val viewModel: ProjectViewModel by activityViewModels()

}