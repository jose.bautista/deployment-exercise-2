package com.example.androidexercise1

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.androidexercise1.data.ProjectViewModel
import kotlinx.android.synthetic.main.fragment_1.view.*


class Fragment1 : Fragment() {
    private lateinit var myViewModel : ProjectViewModel


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
    savedInstance: Bundle?): View? {
        //super.onViewCreated(view, savedInstanceState)
        val view = inflater.inflate(R.layout.fragment_1,container,false)


        val adapter = ProjectAdapter()
        val rcv = view.findViewById<RecyclerView>(R.id.recycle)
        rcv?.adapter = adapter

        myViewModel = ViewModelProvider(this).get(ProjectViewModel::class.java)
        viewModel.getProjects.observe(viewLifecycleOwner, Observer { project -> adapter.setData(project)})

//        {
//            findNavController().navigate(Fragment1Directions.actionFragment1ToFragment2(it.long_description, it.big_Image))
//        }

        view.floatingActionButton.setOnClickListener{
            findNavController().navigate(R.id.action_fragment1_to_addFragment)}
    return view
    }

    private val viewModel: ProjectViewModel by activityViewModels()

}